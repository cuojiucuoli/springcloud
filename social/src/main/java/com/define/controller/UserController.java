package com.define.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

	@RequestMapping("/user/use")
	@ResponseBody
	public Map<String,Object> getUserInfo() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("username", 123);
		map.put("password", 123);
		return map;
	}
	
	
	@RequestMapping("/other")
	@ResponseBody
	public Map<String,Object> getDetails() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("address", "柳州市");
		map.put("birthday", "19920909");
		return map;
	}
}
