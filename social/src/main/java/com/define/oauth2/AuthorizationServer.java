package com.define.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
@Configuration
@EnableAuthorizationServer

public class AuthorizationServer extends AuthorizationServerConfigurerAdapter{

	@Autowired
	BCryptPasswordEncoder bp;
	
	@Autowired
    private AuthenticationManager authenticationManager;
	
	@Autowired
	TokenStore tokenStore;

	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().withClient("client_1")
        .resourceIds("user")
        .authorizedGrantTypes("client_credentials", "refresh_token")
        .scopes("select")
        .authorities("client")
        .secret(bp.encode("123456"))
        .and().withClient("client_2")
        .resourceIds("user")
        .authorizedGrantTypes("password", "refresh_token")
        .scopes("select")
        .authorities("client")
        .secret(bp.encode("123456"))
        .and().withClient("wy")
        .authorizedGrantTypes("authorization_code","refresh_token")
        .scopes("select")
        .secret(bp.encode("123"));
  
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore).authenticationManager(authenticationManager);
	}
	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}              
	
}
