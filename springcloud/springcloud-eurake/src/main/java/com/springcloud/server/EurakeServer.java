package com.springcloud.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
@SpringBootApplication
@EnableEurekaServer //EurekaServer的启动类，开启后接收微服务注册进来
public class EurakeServer {

	public static void main(String[] args) {
		SpringApplication.run(EurakeServer.class, args);
	}
}
