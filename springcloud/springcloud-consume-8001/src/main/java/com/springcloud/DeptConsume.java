package com.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class DeptConsume {

	public static void main(String[] args) {
		SpringApplication.run(DeptConsume.class, args);
	}
}
