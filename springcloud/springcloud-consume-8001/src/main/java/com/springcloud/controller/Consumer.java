package com.springcloud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.springcloud.bean.Dept;

@RestController
public class Consumer {
	
	private static final String REST_URL_PREFIX = "http://localhost:8001/dept";
	
	@Autowired
	RestTemplate rest;
	@RequestMapping(value="/consumer/dept/add")
	public boolean add(Dept dept) {
		return rest.postForObject(REST_URL_PREFIX+"/add", dept, Boolean.class);
	}
	
	@RequestMapping(value="/consumer/dept/get/{id}")
	public Dept get(@PathVariable Long id) {
		return rest.getForObject(REST_URL_PREFIX+"/get/"+id,Dept.class,id);
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/consumer/dept/list")
	public List<Dept> list() {
		return rest.getForObject(REST_URL_PREFIX+"/list", List.class);
	}
}
