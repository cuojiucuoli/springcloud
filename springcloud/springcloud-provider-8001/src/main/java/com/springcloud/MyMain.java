package com.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@ComponentScan
public class MyMain {

	public static void main(String[] args) {
		SpringApplication.run(MyMain.class, args);
	}
}
