package com.springcloud.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.springcloud.MyMain;
import com.springcloud.bean.Dept;
import com.springcloud.service.DeptService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= {MyMain.class})
public class MyTest {

	@Autowired
	DeptService service;
	
	@Test
	public void M() {
		List<Dept> list = service.list();
		System.out.println(list);
	}
}
