package com.springcloud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcloud.bean.Dept;
import com.springcloud.dao.DeptMapper;

@Service
public class DeptService {

	@Autowired
	DeptMapper dao;
	
	public void add(Dept dept) {
		dao.addDept();
	}
	
	public Dept get(Long id) {
		Dept dept = dao.findById(id);
		return dept;
	}
	
	public List<Dept> list(){
		List<Dept> list = dao.findAll();
		return list;
	}
}
