package com.springcloud.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.springcloud.bean.Dept;

@Mapper
public interface DeptMapper {

	Dept findById(Long deptno);
	List<Dept> findAll();
	void addDept();
}
